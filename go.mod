module gitlab.com/reda.bourial/arithmetic

go 1.20

require github.com/stretchr/testify v1.8.2

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gitlab.com/reda.bourial/catch v1.0.4 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
