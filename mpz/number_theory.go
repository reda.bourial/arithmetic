package mpz

// #include "mpz.h"
import "C"

// OK
// Number Theoretic Functions

// int pmpz_invert(unsafe_mpz rop, const unsafe_mpz op1, const unsafe_mpz op2);
func (m Mpz) Invert(op Mpz) (Mpz, C.int) {
	rop := MpzInit()
	r := C.pmpz_invert(rop.Ptr(), m.Ptr(), op.Ptr())
	return rop, r
}

// int pmpz_jacobi(const unsafe_mpz a, const unsafe_mpz b);
func (m Mpz) Jacobi(op Mpz) C.int {
	return C.pmpz_jacobi(m.Ptr(), op.Ptr())
}

// int pmpz_kronecker(const unsafe_mpz a, const unsafe_mpz b);
func (m Mpz) Kronecker(op Mpz) C.int {
	return C.pmpz_kronecker(m.Ptr(), op.Ptr())
}

// int pmpz_legendre(const unsafe_mpz a, const unsafe_mpz p);
func (m Mpz) Legendre(p Mpz) C.int {
	return C.pmpz_legendre(m.Ptr(), p.Ptr())
}

// int pmpz_probab_prime_p(const unsafe_mpz n, int reps);
func (m Mpz) ProbabPrimeP(reps C.int) C.int {
	return C.pmpz_probab_prime_p(m.Ptr(), reps)
}

// mp_bitcnt_t pmpz_remove(unsafe_mpz rop, const unsafe_mpz op, const mpz_t f);
func (m Mpz) Remove(op Mpz, f Mpz) C.ulong {
	rop := MpzInit()
	return C.pmpz_remove(rop.Ptr(), op.Ptr(), f.Ptr())
}

// unsafe_mpz pmpz_gcd(const unsafe_mpz op1, const unsafe_mpz op2);
func (op1 Mpz) Gcd(op2 Mpz) Mpz {
	ptr := C.pmpz_gcd(op1.Ptr(), op2.Ptr())
	return mpzFromPtr(ptr)
}

// unsafe_mpz pmpz_lcm(const unsafe_mpz op1, const unsafe_mpz op2);
func (op1 Mpz) Lcm(op2 Mpz) Mpz {
	ptr := C.pmpz_lcm(op1.Ptr(), op2.Ptr())
	return mpzFromPtr(ptr)
}

// unsafe_mpz pmpz_nextprime(const unsafe_mpz op);
func (op Mpz) NextPrime() Mpz {
	ptr := C.pmpz_nextprime(op.Ptr())
	return mpzFromPtr(ptr)
}

// void pmpz_gcdext(unsafe_mpz g, unsafe_mpz s, unsafe_mpz t, const unsafe_mpz a, const unsafe_mpz b);
func GcdExt(a Mpz, b Mpz) (g Mpz, s Mpz, t Mpz) {
	g, s, t = MpzInit(), MpzInit(), MpzInit()
	C.pmpz_gcdext(g.Ptr(), s.Ptr(), t.Ptr(), a.Ptr(), b.Ptr())
	return g, s, t
}
