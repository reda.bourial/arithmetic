package mpz

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMisc(t *testing.T) {

	t.Run("Even", func(t *testing.T) {
		assert.Equal(t, false, MpzFromString("49", 10).Even())
		assert.Equal(t, true, MpzFromString("48", 10).Even())
	})

	t.Run("Odd", func(t *testing.T) {
		assert.Equal(t, true, MpzFromString("49", 10).Odd())
		assert.Equal(t, false, MpzFromString("48", 10).Odd())
	})
	t.Run("FitsSintP", func(t *testing.T) {
		assert.Equal(t, true, MpzFromString("1", 10).FitsSintP())
		assert.Equal(t, false, MpzFromString("9223372036854775808", 10).FitsSintP())
	})

	t.Run("FitsSlongP", func(t *testing.T) {
		assert.Equal(t, true, MpzFromString("1", 10).FitsSlongP())
		assert.Equal(t, false, MpzFromString("9223372036854775808", 10).FitsSlongP())
	})

	t.Run("FitsSshortP", func(t *testing.T) {
		assert.Equal(t, true, MpzFromString("1", 10).FitsSshortP())
		assert.Equal(t, false, MpzFromString("9223372036854775808", 10).FitsSshortP())
	})

	t.Run("FitsSshortP", func(t *testing.T) {
		assert.Equal(t, true, MpzFromString("1", 10).FitsSshortP())
		assert.Equal(t, false, MpzFromString("9223372036854775808", 10).FitsSshortP())
	})

	t.Run("FitsUintP", func(t *testing.T) {
		assert.Equal(t, true, MpzFromString("1", 10).FitsUintP())
		assert.Equal(t, false, MpzFromString("9223372036854775808", 10).FitsUintP())
	})
	t.Run("FitsUlongP", func(t *testing.T) {
		assert.Equal(t, true, MpzFromString("1", 10).FitsUlongP())
		assert.Equal(t, false, MpzFromString("100000000000000000000000000000000000000000000", 10).FitsUlongP())
	})
	t.Run("fitsUshortP", func(t *testing.T) {
		assert.Equal(t, true, MpzFromString("1", 10).FitsUshortP())
		assert.Equal(t, false, MpzFromString("9223372036854775808", 10).FitsUshortP())
	})

}
