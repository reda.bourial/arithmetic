package mpz

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLogic(t *testing.T) {

	q1 := MpzFromString("49", 10)
	q2 := MpzFromString("77", 10)
	q3 := MpzFromString("24", 10)

	t.Run("MpzTstBit", func(t *testing.T) {
		assert.Equal(t, true, q1.TstBit(0))
		assert.Equal(t, false, q1.TstBit(1))
		assert.Equal(t, false, q1.TstBit(2))
		assert.Equal(t, false, q1.TstBit(3))
		assert.Equal(t, true, q1.TstBit(4))
		assert.Equal(t, true, q1.TstBit(5))
		assert.Equal(t, false, q1.TstBit(6))
		assert.Equal(t, false, q1.TstBit(7))
		assert.Equal(t, false, q1.TstBit(8))
		assert.Equal(t, true, q2.TstBit(0))
		assert.Equal(t, false, q2.TstBit(1))
		assert.Equal(t, true, q2.TstBit(2))
		assert.Equal(t, true, q2.TstBit(3))
		assert.Equal(t, false, q2.TstBit(4))
		assert.Equal(t, false, q2.TstBit(5))
		assert.Equal(t, true, q2.TstBit(6))
		assert.Equal(t, false, q2.TstBit(7))
		assert.Equal(t, false, q2.TstBit(8))
		assert.Equal(t, false, q2.TstBit(9))
		assert.Equal(t, false, q2.TstBit(10))
	})

	t.Run("HamDist", func(t *testing.T) {
		assert.Equal(t, uint(5), q1.HamDist(q2))
		assert.Equal(t, uint(5), q2.HamDist(q1))
		assert.Equal(t, uint(3), q1.HamDist(q3))
		assert.Equal(t, uint(3), q3.HamDist(q1))
	})

	t.Run("PopCount", func(t *testing.T) {
		assert.Equal(t, uint(3), q1.PopCount())
		assert.Equal(t, uint(4), q2.PopCount())
		assert.Equal(t, uint(2), q3.PopCount())
	})
	t.Run("And", func(t *testing.T) {
		assert.Equal(t, "1", q1.And(q2).String())
		assert.Equal(t, "1", q2.And(q1).String())

		assert.Equal(t, "16", q1.And(q3).String())
		assert.Equal(t, "16", q3.And(q1).String())

		assert.Equal(t, "8", q2.And(q3).String())
		assert.Equal(t, "8", q3.And(q2).String())
	})

	t.Run("Com", func(t *testing.T) {
		assert.Equal(t, "-50", q1.Com().String())
		assert.Equal(t, "-78", q2.Com().String())
		assert.Equal(t, "-25", q3.Com().String())
	})

	t.Run("IOr", func(t *testing.T) {
		assert.Equal(t, "125", q1.IOr(q2).String())
		assert.Equal(t, "125", q2.IOr(q1).String())

		assert.Equal(t, "57", q1.IOr(q3).String())
		assert.Equal(t, "57", q3.IOr(q1).String())

		assert.Equal(t, "93", q2.IOr(q3).String())
		assert.Equal(t, "93", q3.IOr(q2).String())
	})

	t.Run("Xor", func(t *testing.T) {
		assert.Equal(t, "124", q1.Xor(q2).String())
		assert.Equal(t, "124", q2.Xor(q1).String())

		assert.Equal(t, "41", q1.Xor(q3).String())
		assert.Equal(t, "41", q3.Xor(q1).String())

		assert.Equal(t, "85", q2.Xor(q3).String())
		assert.Equal(t, "85", q3.Xor(q2).String())
	})

	t.Run("ClrBit", func(t *testing.T) {
		assert.Equal(t, "48", q1.ClrBit(0).String())
		assert.Equal(t, "49", q1.ClrBit(3).String())
	})

	t.Run("ComBit", func(t *testing.T) {
		assert.Equal(t, "48", q1.ComBit(0).String())
		assert.Equal(t, "49", q1.ComBit(2).String())
	})

	t.Run("SetBit", func(t *testing.T) {
		assert.Equal(t, "4145", q1.SetBit(12).String())
	})

}
