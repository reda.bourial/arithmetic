package mpz

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExp(t *testing.T) {
	mpz19 := MpzFromString("19", 10)
	mpz16 := MpzFromString("16", 10)

	t.Run("PowUi", func(t *testing.T) {
		q := mpz19.PowUi(2)
		assert.Equal(t, "361", q.String())
		q = mpz16.PowUi(0)
		assert.Equal(t, "1", q.String())
	})
	t.Run("PowM", func(t *testing.T) {
		q := mpz19.PowM(MpzFromUi(2), MpzFromUi(100))
		assert.Equal(t, "61", q.String())
		q = mpz16.PowM(MpzFromUi(2), MpzFromUi(5))
		assert.Equal(t, "1", q.String())

	})
	t.Run("PowMSec", func(t *testing.T) {
		q := mpz19.PowMSec(MpzFromUi(2), MpzFromUi(99))
		assert.Equal(t, "64", q.String())
		q = mpz16.PowMSec(MpzFromUi(2), MpzFromUi(5))
		assert.Equal(t, "1", q.String())
	})
	t.Run("PowMUi", func(t *testing.T) {
		q := mpz19.MpzPowMUi(2, MpzFromUi(100))
		assert.Equal(t, "61", q.String())
		q = mpz16.MpzPowMUi(2, MpzFromUi(5))
		assert.Equal(t, "1", q.String())
	})
}
