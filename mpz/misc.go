package mpz

// #include "mpz.h"
import "C"

// Miscellaneous Functions
// int pmpz_even_p(const unsafe_mpz op);
func (op Mpz) Even() bool {
	return C.pmpz_even_p(op.Ptr()) != 0
}

// Miscellaneous Functions
// int pmpz_even_p(const unsafe_mpz op);
func (op Mpz) Odd() bool {
	return C.pmpz_odd_p(op.Ptr()) != 0
}

// int pmpz_fits_sint_p(const unsafe_mpz op);
func (op Mpz) FitsSintP() bool {
	return C.pmpz_fits_sint_p(op.Ptr()) != 0
}

// int pmpz_fits_slong_p(const unsafe_mpz op);
func (op Mpz) FitsSlongP() bool {
	return C.pmpz_fits_slong_p(op.Ptr()) != 0
}

// int pmpz_fits_sshort_p(const unsafe_mpz op);
func (op Mpz) FitsSshortP() bool {
	return C.pmpz_fits_sshort_p(op.Ptr()) != 0
}

// int pmpz_fits_uint_p(const unsafe_mpz op);
func (op Mpz) FitsUintP() bool {
	return C.pmpz_fits_uint_p(op.Ptr()) != 0
}

// int pmpz_fits_ulong_p(const unsafe_mpz op);
func (op Mpz) FitsUlongP() bool {
	return C.pmpz_fits_ulong_p(op.Ptr()) != 0
}

// int pmpz_fits_ushort_p(const unsafe_mpz op);
func (op Mpz) FitsUshortP() bool {
	return C.pmpz_fits_ushort_p(op.Ptr()) != 0
}
