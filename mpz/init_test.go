package mpz

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInit(t *testing.T) {
	t.Run("MpzInit", func(t *testing.T) {
		q := MpzInit()
		assert.Equal(t, "0", q.String())
	})

	t.Run("MpzFromString", func(t *testing.T) {
		q := MpzFromString("-42", 10)
		assert.Equal(t, "-42", q.String())
	})

	t.Run("MpzFromDouble", func(t *testing.T) {
		q := MpzFromDouble(42)
		assert.Equal(t, "42", q.String())
	})

	t.Run("MpzFromMpz", func(t *testing.T) {
		m1 := MpzFromString("42", 10)
		m2 := MpzFromMpz(m1)
		assert.Equal(t, m2.String(), m1.String())
	})

	t.Run("MpzFromSi", func(t *testing.T) {
		q := MpzFromSi(-42)
		assert.Equal(t, "-42", q.String())
	})

	t.Run("MpzFromUi", func(t *testing.T) {
		q := MpzFromUi(42)
		assert.Equal(t, "42", q.String())
	})

	t.Run("FromUiPowUi", func(t *testing.T) {
		q := MpzFromUiPowUi(10, 2)
		assert.Equal(t, "100", q.String())
	})

	t.Run("Mpz2FacUi", func(t *testing.T) {
		m := Mpz2FacUi(0)
		assert.Equal(t, "1", m.String())
		m = Mpz2FacUi(1)
		assert.Equal(t, "1", m.String())
		m = Mpz2FacUi(2)
		assert.Equal(t, "2", m.String())
		m = Mpz2FacUi(3)
		assert.Equal(t, "3", m.String())
		m = Mpz2FacUi(4)
		assert.Equal(t, "8", m.String())
	})

	t.Run("MpzFacUi", func(t *testing.T) {
		m := MpzFacUi(0)
		assert.Equal(t, "1", m.String())
		m = MpzFacUi(1)
		assert.Equal(t, "1", m.String())
		m = MpzFacUi(2)
		assert.Equal(t, "2", m.String())
		m = MpzFacUi(3)
		assert.Equal(t, "6", m.String())
		m = MpzFacUi(4)
		assert.Equal(t, "24", m.String())
	})

	t.Run("MpzFibUi", func(t *testing.T) {
		m := MpzFibUi(0)
		assert.Equal(t, "0", m.String())
		m = MpzFibUi(1)
		assert.Equal(t, "1", m.String())
		m = MpzFibUi(2)
		assert.Equal(t, "1", m.String())
		m = MpzFibUi(3)
		assert.Equal(t, "2", m.String())
		m = MpzFibUi(4)
		assert.Equal(t, "3", m.String())
	})

	t.Run("MpzLucNumUi", func(t *testing.T) {
		m := MpzLucNumUi(0)
		assert.Equal(t, "2", m.String())
		m = MpzLucNumUi(1)
		assert.Equal(t, "1", m.String())
		m = MpzLucNumUi(2)
		assert.Equal(t, "3", m.String())
		m = MpzLucNumUi(3)
		assert.Equal(t, "4", m.String())
		m = MpzLucNumUi(4)
		assert.Equal(t, "7", m.String())
	})

	t.Run("MpzMFacUiUi", func(t *testing.T) {
		m := MpzMFacUiUi(0, 3)
		assert.Equal(t, "1", m.String())
		m = MpzMFacUiUi(5, 1)
		assert.Equal(t, "120", m.String())
		m = MpzMFacUiUi(5, 2)
		assert.Equal(t, "15", m.String())
	})

	t.Run("MpzPrimordialUi", func(t *testing.T) {
		m := MpzPrimordialUi(0)
		assert.Equal(t, "1", m.String())
		m = MpzPrimordialUi(1)
		assert.Equal(t, "1", m.String())
		m = MpzPrimordialUi(2)
		assert.Equal(t, "2", m.String())
		m = MpzPrimordialUi(3)
		assert.Equal(t, "6", m.String())
		m = MpzPrimordialUi(4)
		assert.Equal(t, "6", m.String())
	})
}
