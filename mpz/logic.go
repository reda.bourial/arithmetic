package mpz

// #include "mpz.h"
import "C"

// Logical and Bit Manipulation Functions
// uint pmpz_tstbit(const unsafe_mpz op, mp_bitcnt_t bit_index);
func (op Mpz) TstBit(bitIndex uint) bool {
	return C.pmpz_tstbit(op.Ptr(), C.ulong(bitIndex)) != 0
}

// mp_bitcnt_t pmpz_hamdist(const unsafe_mpz op1, const unsafe_mpz op2);
func (op1 Mpz) HamDist(op2 Mpz) uint {
	return uint(C.pmpz_hamdist(op1.Ptr(), op2.Ptr()))
}

// mp_bitcnt_t pmpz_popcount(const unsafe_mpz op);
func (op Mpz) PopCount() uint {
	return uint(C.pmpz_popcount(op.Ptr()))
}

// Use TstBit instead
// // mp_bitcnt_t pmpz_scan0(const unsafe_mpz op, mp_bitcnt_t starting_bit);
// func (op Mpz) Scan0(startingBit uint) bool {
// 	return C.pmpz_scan0(op.Ptr(), C.ulong(startingBit)) != 0
// }
//
// // mp_bitcnt_t pmpz_scan1(const unsafe_mpz op, mp_bitcnt_t starting_bit);
// func (op Mpz) Scan1(startingBit uint) bool {
// 	return C.pmpz_scan1(op.Ptr(), C.ulong(startingBit)) != 0
// }

// unsafe_mpz pmpz_and(const unsafe_mpz op1, const unsafe_mpz op2);
func (op1 Mpz) And(op2 Mpz) Mpz {
	ptr := C.pmpz_and(op1.Ptr(), op2.Ptr())
	return mpzFromPtr(ptr)
}

// unsafe_mpz pmpz_com(const unsafe_mpz op);
func (op Mpz) Com() Mpz {
	ptr := C.pmpz_com(op.Ptr())
	return mpzFromPtr(ptr)
}

// unsafe_mpz pmpz_ior(const unsafe_mpz op1, const unsafe_mpz op2);
func (op1 Mpz) IOr(op2 Mpz) Mpz {
	ptr := C.pmpz_ior(op1.Ptr(), op2.Ptr())
	return mpzFromPtr(ptr)
}

// unsafe_mpz pmpz_xor(const unsafe_mpz op1, const unsafe_mpz op2);
func (op1 Mpz) Xor(op2 Mpz) Mpz {
	ptr := C.pmpz_xor(op1.Ptr(), op2.Ptr())
	return mpzFromPtr(ptr)
}

// void pmpz_clrbit(unsafe_mpz rop, mp_bitcnt_t bit_index);
func (op Mpz) ClrBit(bitIndex ...uint) Mpz {
	rop := MpzFromMpz(op)
	for _, idx := range bitIndex {
		C.pmpz_clrbit(rop.Ptr(), C.ulong(idx))
	}
	return rop
}

// void pmpz_combit(unsafe_mpz rop, mp_bitcnt_t bit_index);
func (op Mpz) ComBit(bitIndex ...uint) Mpz {
	rop := MpzFromMpz(op)
	for _, idx := range bitIndex {
		C.pmpz_combit(rop.Ptr(), C.ulong(idx))
	}
	return rop
}

// void pmpz_setbit(const unsafe_mpz op, mp_bitcnt_t bit_index);
func (op Mpz) SetBit(bitIndex ...uint) Mpz {
	rop := MpzFromMpz(op)
	for _, idx := range bitIndex {
		C.pmpz_setbit(rop.Ptr(), C.ulong(idx))
	}
	return rop
}
