
SHELL := /bin/bash
buildDir= ./build
coverPath= ${buildDir}/coverage.out

clean:
	rm -rf $(buildDir)/*

test:
	go test ./mpz -coverprofile=${coverPath}

coverage:
	go tool cover -func=${coverPath}

ci_coverage:
	make coverage | grep "total:"
	make coverage | grep "total:" |grep "100.0%"

html_coverage:test	
	go tool cover -html=${coverPath}
