
#ifndef ARTITHMETIC_MEMORY_MANAGEMENT_H
#define ARTITHMETIC_MEMORY_MANAGEMENT_H
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>

#if UINTPTR_MAX == 0xffffffff
define PTR_ALLOCATION_MESSAGE
    "Pointer malloc(ed) (%s): 0x%08lx\r\n" define PTR_FREE_MESSAGE
    "Pointer free(ed) (%s):   0x%08lx\r\n"
#elif UINTPTR_MAX == 0xffffffffffffffff
#define PTR_ALLOCATION_MESSAGE "Pointer malloc(ed) (%s): 0x%016lx\r\n"
#define PTR_FREE_MESSAGE "Pointer free(ed) (%s):   0x%016lx\r\n"
#endif

#define DEBUG 0
// Macros for debugging memory management
#define MALLOC_PROXY(type, typename, var, size)                                \
  type var = (type)malloc(size);                                               \
  if (DEBUG) {                                                                 \
    printf(PTR_ALLOCATION_MESSAGE, typename, var);                     \
    fflush(stdout);                                                            \
  }

#define FREE_PROXY(type, ptr)                                                  \
  if (DEBUG) {                                                                 \
    printf(PTR_FREE_MESSAGE, type, ptr);                               \
    fflush(stdout);                                                            \
  }                                                                            \
  free(ptr);

#define FREE_PROXY_DECLARATION

#endif /* ARTITHMETIC_MEMORY_MANAGEMENT_H */